local jwt = require "resty.jwt"
local jwt_token = ngx.var.arg_jwt
if jwt_token then
    ngx.header['Set-Cookie'] = "jwt=" .. jwt_token
else
    jwt_token = ngx.var.cookie_jwt
end

local f = assert(io.open("/etc/nginx/conf.d/public.cer", "rb"))
local cert = f:read("*all")
f:close()

local jwt_obj = jwt:verify(cert, jwt_token)

if not jwt_obj["verified"] then
    local site = ngx.var.scheme .. "://" .. ngx.var.http_host;
    local args = ngx.req.get_uri_args();

    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.header['Content-Type'] = "text/plain";
    ngx.say("Unauthorized");
    ngx.exit(ngx.HTTP_OK)
end
